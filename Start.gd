extends Node2D

var payment

const TEST_ITEM_SKU = ['coinmonkey', 'inapp2']

var purchase_token = null

func _ready ():
	MobileAds.connect ("initialization_complete", self, '_ads_loaded')
	MobileAds.connect ("interstitial_loaded", self, '_interstitial_ad_loaded')
	MobileAds.connect ("rewarded_ad_loaded", self, '_rewarded_loaded')
	
	if Engine.has_singleton ("GodotGooglePlayBilling"):
		payment = Engine.get_singleton ("GodotGooglePlayBilling")
		
		payment.connect ("connected", self, "_on_connected") # No params
		payment.connect ("disconnected", self, "_on_disconnected") # No params
		payment.connect ("connect_error", self, "_on_connect_error") # Response ID (int), Debug message (string)
		payment.connect ("purchases_updated", self, "_on_purchases_updated") # Purchases (Dictionary[])
		payment.connect ("purchase_error", self, "_on_purchase_error") # Response ID (int), Debug message (string)
		payment.connect ("sku_details_query_completed", self, "_on_sku_details_query_completed") # SKUs (Dictionary[])
		payment.connect ("sku_details_query_error", self, "_on_sku_details_query_error") # Response ID (int), Debug message (string), Queried SKUs (string[])
		payment.connect ("purchase_acknowledged", self, "_on_purchase_acknowledged") # Purchase token (string)
		payment.connect ("purchase_acknowledgement_error", self, "_on_purchase_acknowledgement_error") # Response ID (int), Debug message (string), Purchase token (string)
		payment.connect ("purchase_consumed", self, "_on_purchase_consumed") # Purchase token (string)
		payment.connect ("purchase_consumption_error", self, "_on_purchase_consumption_error") # Response ID (int), Debug message (string), Purchase token (string)

		payment.startConnection ()
		
		$MarginContainer/VBoxContainer/MarginContainer5/InApp.disabled = false
		
	else:
		$MarginContainer/VBoxContainer/MarginContainer4/Label.text = "Error de IN APP"

# Inicia Ad Mobs

func _ads_loaded (status, adapter_name) :
	if status == MobileAds.INITIALIZATION_STATUS.READY :
		MobileAds.load_banner () # Carga un banner en cuanto se activa el sistema
		MobileAds.load_interstitial ()
		MobileAds.load_rewarded ()

func _interstitial_ad_loaded ():
	$MarginContainer/VBoxContainer/MarginContainer/Interstitial.disabled = false
	
func _rewarded_loaded ():
	$MarginContainer/VBoxContainer/MarginContainer2/Rewarded.disabled = false

func _on_Interstitial_pressed ():
	MobileAds.show_interstitial () # Muestra el video

func _on_Rewarded_pressed():
	MobileAds.show_rewarded ()

# Inicia In App de gugul

func _on_connected ():
	$MarginContainer/VBoxContainer/MarginContainer4/Label.text = "Purchase Manager Connected"
	payment.querySkuDetails (TEST_ITEM_SKU, 'inapp')

func _on_sku_details_query_completed (sku_details):
	for available_sku in sku_details:
		print(available_sku)

func _on_purchases_updated (items):
	for item in items:
		if !item.is_acknowledged:
			payment.acknowledgedPurchase (item.pruchase_token)
			$MarginContainer/VBoxContainer/MarginContainer4/Label.text = "Acknowledge"
		if items.size () < 0:
			purchase_token = items [items.size () - 1].purchase_token

func _on_InApp_pressed ():
	var response = payment.purchase ("coinmonkey")
	
	if response.status != OK:
		$MarginContainer/VBoxContainer/MarginContainer4/Label.text = response.status
	else:
		$MarginContainer/VBoxContainer/MarginContainer4/Label.text = response.status
		$MarginContainer/VBoxContainer/MarginContainer6/Moneda.disabled = false


func _on_Moneda_pressed():
	if purchase_token == null:
		$MarginContainer/VBoxContainer/MarginContainer4/Label.text = "No tienes moneda, compra"
	else:
		$MarginContainer/VBoxContainer/MarginContainer4/Label.text = 'Se gasto la moneda'
		payment.consumePurchase(purchase_token)
		$MarginContainer/VBoxContainer/MarginContainer6/Moneda.disabled = true
